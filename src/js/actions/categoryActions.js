import { FETCH_ITEMS } from '../constants/allConstants';

export const getCategoryProducts = cat => {
  return dispatch =>{
    var url = "http://localhost:8000/products/"+cat;
    fetch(url).then((res) => res.json()).then((data) => {
    console.log("hitting api.........");
    console.log(data);
     dispatch({
      type: FETCH_ITEMS,
      payload: data
    });
  })

  // const fetchSuccess = todo => ({
  //   type: FETCH_ITEMS,
  //   payload: todo
  // });
  }
}