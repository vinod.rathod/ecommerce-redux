import { REMOVE_ITEM } from '../constants/allConstants';

export const removeItem = (itemNumber) => {
    console.log("itemNumber is: "+itemNumber);
    // debugger;
    return dispatch =>{
      var obj = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
           body: JSON.stringify({
             itemid : itemNumber
           })
        };
    var url = "http://localhost:8000/removeitems";
      //   debugger;
    fetch(url,obj).then((res) => res.json()).then((data) => {
    console.log("hitting /removeitem api.........");
    console.log(data);
     dispatch({
      type: REMOVE_ITEM,
      payload: data
    });
  })
  }
  }