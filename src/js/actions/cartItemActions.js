import { SEND_CART_ITEMS } from '../constants/allConstants';
import { REMOVE_ITEM } from '../constants/allConstants';

export const sendCartItemActions = (cartItem) => {
    // debugger;
    console.log("in cartItemActions "+cartItem);
    return dispatch =>{
        var obj = {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
             body: JSON.stringify(cartItem)
          };
      var url = "http://localhost:8000/postitems";
      fetch(url,obj).then((res) => {
        if(res.status == 201){
          alert("Item has been added to the cart");
        }
        res.json();
        }).then((data) => {
      console.log("hitting /postitems api.........");
      console.log(data);
       dispatch({
        type: SEND_CART_ITEMS,
        payload: data
      });
    })
}
}

// export const removeItem = (itemNumber) => {
//   return dispatch =>{
//     var obj = {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json',
//           'Accept': 'application/json'
//         },
//          body: JSON.stringify({
//            itemid : itemNumber
//          })
//       };
//   var url = "http://localhost:8000/removeitems";
//     //   debugger;
//   fetch(url,obj).then((res) => res.json()).then((data) => {
//   console.log("hitting /removeitem api.........");
//   console.log(obj.body);
//    dispatch({
//     type: REMOVE_ITEM,
//     payload: state.cart.filter((item) => !== item)
//   });
// })
// }
// }