import { GET_MY_CART } from '../constants/allConstants';
import { USER_ID } from '../constants/allConstants'

export const cartActions = () => {
    return dispatch =>{
      var url = "http://localhost:8000/mycart/"+USER_ID;
      fetch(url).then((res) => res.json()).then((data) => {
      console.log("hitting /mycart api.........");
      console.log(data);
       dispatch({
        type: GET_MY_CART,
        payload: data
      });
    })
}
}