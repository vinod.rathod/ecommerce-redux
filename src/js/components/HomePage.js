import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';
import { connect } from 'react-redux';
import { getCategoryProducts } from '../actions/categoryActions';

class HomePage extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <div>
                <div className="image1">
                    <img src="https://shop.polymer-project.org/es6-bundled/images/mens_outerwear.jpg" alt="mensouterwear" />
                </div>
                <div className="box1">
                    <h4>Men's Outerwear</h4>
                    <NavLink to={`/products/mensouterwear`}><button id="shop-button" onClick={ ()=> this.props.getCategoryProducts('mensouterwear')}>SHOP NOW</button></NavLink>
                </div>
                <div className="image1">
                    <img src="https://shop.polymer-project.org/es6-bundled/images/ladies_outerwear.jpg" alt="mensouterwear" />
                </div>
                <div className="box1">
                    <h4>Ladies Outerwear</h4>
                    <NavLink to={`/products/womensouterwear`}><button id="shop-button" onClick={ ()=> this.props.getCategoryProducts('womensouterwear')}>SHOP NOW</button></NavLink>
                </div>
                <div className="doubleimage">
                    <div className="first">
                        <img src="https://shop.polymer-project.org/es6-bundled/images/mens_tshirts.jpg" alt="mens_t-shirt" />
                        <h4>Men's T-shirt</h4>
                        <NavLink to={`/products/menstshirt`}><button id="shop-button" onClick={ ()=> this.props.getCategoryProducts('menstshirt')}>SHOP NOW</button></NavLink>
                    </div>
                    <div className="second">
                        <img src="https://shop.polymer-project.org/es6-bundled/images/ladies_tshirts.jpg" alt="ladies_t-shirt" />
                        <h4>Ladies t-shirt</h4>
                        <NavLink to={`/products/womenstshirt`}><button id="shop-button" onClick={ ()=> this.props.getCategoryProducts('womenstshirt')}>SHOP NOW</button></NavLink>
                    </div>

                </div>
            </div>
        );
    }
}

// export default HomePage;

function mapStateToProps(state, props) {
    return {
        items: state.products 
    };
}

const mapDispatchToProps = (dispatch)=>({
    getCategoryProducts: (category)=>dispatch(getCategoryProducts(category)),
});



export default connect(mapStateToProps,mapDispatchToProps)(HomePage);