import React, { Component } from 'react';
import { getCategoryProducts } from '../actions/categoryActions';
import { connect } from 'react-redux';
import fetchData from '../reducers/getProducts';
import { NavLink } from 'react-router-dom';

class GridComponent extends Component {
    constructor(props) {
        super(props);
        // console.log(props);
    }
    componentDidMount() {
    }

    render() {
        var { product } = {};
        var grid = [];
        if (this.props.items.hasOwnProperty("products")) {
            // console.log(this.props.items.products[0]);
            product = this.props.items.products[0].name;
            this.props.items.products.forEach(element => {
                grid.push(
                    <NavLink to={`/details/${element.id}`} key={element.id}>
                        <div className="flexbox">
                            <img src={element.url} alt="img"></img>
                            <h4>{element.name}</h4>
                            <h4>{element.price}</h4>
                        </div>
                    </NavLink>
                );
            });
        }
        return (
            <div>
                <h4 className="boxname">Men's Outerwear</h4>
                <div className="gridcontainer" id="grid-box">{grid}</div>
            </div>
        );
    }
}

// export default GridComponent;
function mapStateToProps(state, props) {
    // { console.log("in maptostate " + state.products) }
    return {
        items: state.products
    };
}

export default connect(mapStateToProps)(GridComponent);