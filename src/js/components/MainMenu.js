import React, { Component } from 'react';
// import { BrowserRouter, Route, Link } from 'react-router-dom';
import {NavLink} from 'react-router-dom';
import { connect } from 'react-redux';
import { getCategoryProducts } from '../actions/categoryActions';
import { FaShoppingCart } from 'react-icons/fa';
import { cartActions } from '../actions/cartActions';
import { GET_MY_CART } from '../constants/allConstants';

// import getCategoryProducts from '../actions/categoryActions';

class MainMenu extends Component {
  constructor(props){
      super(props);
  }
    render() {
        return (
            <div>
                <div className="main-nav">
                
                    <NavLink className="links" to={`/`} key={'1'}></NavLink>
                    <NavLink className="links" to={`/`} key={'2'}><strong className="title">SHOP</strong></NavLink>
                    {/* <NavLink className="links" to={`/cart`} key={'3'}><FaShoppingCart /></NavLink> */}
                    <NavLink className="links" to={`/cart`}><button id="button-links" onClick={ ()=> this.props.cartActions()}><FaShoppingCart /></button></NavLink>
                    
                </div>
                <div className="menu">
                    {/* <NavLink className="links" to={`/${'Mens jacket'}`} >Mens's Outerweear</NavLink>
                    <NavLink className="links" to={`/${'Womens jacket'}`} >Womens's Outerweear</NavLink>
                    <NavLink className="links" to={`/${'Mens t-shirt'}`} >Men's T-shirt</NavLink>
                    <NavLink className="links" to={`/${'Womens t-shirt'}`} >Womens's T-shirt</NavLink> */}
                    {/* <NavLink className="links" to={`/products`}  onClick={ ()=> this.props.getCategoryProducts('mensouterwear')}>Mens's Outerweear</NavLink>
                    <NavLink className="links" to={`/products`}  onClick={ () => this.props.getCategoryProducts('womensouterwear') }>Womens's Outerweear</NavLink>
                    <NavLink className="links" to={`/products`}  onClick={ () => this.props.getCategoryProducts('menstshirt')  } >Men's T-shirt</NavLink>
                    <NavLink className="links" to={`/products`}  onClick={ () => this.props.getCategoryProducts('womenstshirt')  } >Womens's T-shirt</NavLink> */}

                    <NavLink className="links" to={`/products/mensouterwear`}><button id="button-links" onClick={ ()=> this.props.getCategoryProducts('mensouterwear')}>Mens's Outerwear</button></NavLink>
                    <NavLink className="links" to={`/products/womensouterwear`}><button id="button-links" onClick={ ()=> this.props.getCategoryProducts('womensouterwear')}>Womens's Outerwear</button></NavLink>
                    <NavLink className="links" to={`/products/mentshirt`}><button id="button-links" onClick={ ()=> this.props.getCategoryProducts('menstshirt')}>Mens's T-shirt</button></NavLink>
                    <NavLink className="links" to={`/products/womenstshirt`}><button id="button-links" onClick={ ()=> this.props.getCategoryProducts('womenstshirt')}>Womens's T-shirt</button></NavLink>
                </div>
            </div>
        )
    }
}
// export default MainMenu;


function mapStateToProps(state, props) {
    return {
        items: state.products 
    };
}

const mapDispatchToProps = (dispatch)=>({
    getCategoryProducts: (category)=>dispatch(getCategoryProducts(category)),
    cartActions: () => dispatch(cartActions()),
});



export default connect(mapStateToProps,mapDispatchToProps)(MainMenu);