import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getCategoryProducts } from '../actions/categoryActions';
import { sendCartItemActions } from '../actions/cartItemActions';
// import Link, { LinkedCompnent } from 'valuelink';
// import { Input } from 'valuelink/tags';
// import  linkAt  from 'valuelink';

class Details extends Component {
    constructor(state) {
        super(state);
        
        this.state = {
            productId: this.props.match.params.id,
            quantity: '',
            size: ''
        }
        // this.handleSubmit = this.handleSubmit.bind(this);
        // this.handleChange = this.handleChange.bind(this);
    }

    handleQuantityChange = (e) =>{ 
        e.preventDefault();
        this.setState({quantity: e.target.value});
      }

      _handleSizeChange = (event) => {
          event.preventDefault();
        this.setState({ size: event.target.value })
      }      

    handleSubmit = (event) => {
        event.preventDefault();
        var data = {
            productId: this.state.productId,
            quantity: this.state.quantity,
            size: this.state.size
        };
        //  debugger;
        console.log(data);
        // debugger;
        this.props.sendCartItemActions(data);
    }
    
    // shouldComponentUpdate(nextProps, nextState) {
    //     return true;
    //   }

    render() {
        console.log("queryParam value " + this.props.match.params.id);
        console.log(this.state);
        var product = {};
        if (this.props.items.hasOwnProperty("products")) {
            console.log(this.props.items.products[0]);
            product = this.props.items.products[0].name;
            this.props.items.products.forEach(element => {
                if (element.id == this.props.match.params.id) {
                    product = element;
                }
            });
        }
        
        let scope = this;

        return (

            <div className="detail-flex-box">
                <div id="image">
                    <img src={product.url} alt="img"></img>
                </div>
                <div className="name" id="pname">
                    <form name="cart-items" onSubmit={this.handleSubmit} >
                        {/* <input type="text" name="productid"  value={this.props.match.params.id}></input> */}
                        <h4>{product.name}</h4>
                        <h5>{product.price}</h5>
                        <select  name ="size" value={scope.state.size} placeholder="Size" className="size" id="size-menu"
                            onChange={this._handleSizeChange}>
                            <option value="" disabled selected >Size</option>
                            <option value="S" className="options">S</option>
                            <option value="M" className="options">M</option>
                            <option value="L" className="options">L</option>
                            <option value="XL" className="options">XL</option>
                            <option value="XXl" className="options">XXL</option>
                        </select>
                        <select name="quantity" placeholder="Quantity" id="quantity" value = {scope.state.quanity}  onChange={this.handleQuantityChange}>
                            <option value="" disabled selected >Quantity</option>
                            <option value="1" className="options">1</option>
                            <option value="2" className="options">2</option>
                            <option value="3" className="options">3</option>
                            <option value="4" className="options">4</option>
                            <option value="5" className="options">5</option>
                        </select>
                        <h5>Description</h5>

                        <div id="desc">
                            <p>{product.description}</p>
                        </div>

                        <br /><br /> Features:
                    <br /><br />
                        <ul>
                            <li>100% combed, ringspun cotton.</li>
                            <li>Extra soft, lightweight fabric.</li>
                            <li><strong>Slim fit. Runs small. </strong></li>
                            <li>Available in aqua with the white Google logo screenprinted at center chest.</li>
                        </ul>
                        {/* <button className="button" id="btn" onClick={ ()=> this.props.cartActions() }>PING TO WISHLIST</button> */}
                        <button type="submit" className="button" id="btn" >ADD TO CART</button>
                    </form>
                </div>
            </div>
        );
    }
}

// export default Details;

function mapStateToProps(state, props) {
    return {
        items: state.products
    };
}

const mapDispatchToProps = (dispatch)=>({
    getCategoryProducts: (category)=>dispatch(getCategoryProducts(category)),
    sendCartItemActions: (params) => dispatch(sendCartItemActions(params)),
});



export default connect(mapStateToProps, mapDispatchToProps)(Details);

// export default connect(mapStateToProps,null)(Details);