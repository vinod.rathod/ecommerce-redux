import React, {Component} from 'react';

class PaymentsPage extends Component {
    render(){
        return(
            
             <div>   
                <div className="center">
                    <h4>CHECKOUT</h4>
                </div>
                
                <div className="checkout-flex">
                    <div>
                        <p className="heading">Account Information</p>
                        
                            <input type="text" name="email" placeholder="Email" />
                            <input type="text" name="phonenumber" placeholder="Phone" />
                            <p className="heading">Shipping address</p>
                            <input type="text" name="address" placeholder="Address" />
                            <input type="text" name="city" placeholder="City" />

                            <div className="small-flex">
                                <div><input type="text" name="state" placeholder="State/Province" /></div>
                                <div><input type="text" name="zipcode" placeholder="Zip/Postal code" /></div>
                            </div>
                            
                            Country
                            <select className="country">
                                <option name="india" value="india" selected>India</option>
                                <option name="usa" value="usa">USA</option>
                                <option name="canada" value="canada">Canada</option>
                            </select>
                    </div> 

                    <div>
                        <p className="heading">Payment Method</p>
                        <input type="text" name="card-holder-name" placeholder="Card Holder Name" />
                        <input type="text" name="card-number" placeholder="Card Number" />
                        <p>Expiry</p>
                        <input type="date" name="expiry" placeholder="Expiry date" />
                        <p><b>Order Summary</b></p>
                        <div className="order-detail">
                            ABCDEF<br />
                            ABCDEF<br />
                            ABCDEF<br />
                        </div>
                        
                        <a href=""><b>Place Order</b></a>
                    </div>
                </div>
                
            </div>
        );
    }
}

export default PaymentsPage;