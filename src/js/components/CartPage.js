import React, { Component } from 'react';
import { connect } from 'react-redux';
import { IoIosClose } from 'react-icons/io';
import { removeItem } from '../actions/removeitem';
// import { getCart } from '../reducers/getCart';

class CartPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // allProducts: {},
            totalAmount: '',
            cartItems: ''
        }
    }

    shouldComponentUpdate(nextProps, nextState){
        return true;
    }
    // componentWillReceiveProps(nextProps){
    //     if(nextProps.item.cartItems.length !== this.props.items.cartItems.length){
    //     }
    //   }

    render() {
        console.log(this.props.items.cartItems);
        // this.myCartProducts();
        var products = [];
        var totalAmt=0;
        if (this.props.items.hasOwnProperty("cartItems")) {
            var cartItems = this.props.items.cartItems;
            cartItems.forEach(element => {
                products.push(
                    <div className="cart-flex-box" key={element.itemid}>
                        <img src={element.url} alt="image" />
                        <p className="product-name">{element.name}</p>
                        <div className="flex-box1">
                            <label htmlFor="qauntity">Quantity:</label>
                            <select className="quantity" id="quantity" defaultValue={element.quantity}>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                        <div className="flex-box2">
                            <label htmlFor="size">Size:</label>
                            <select className="size" id="size" defaultValue={element.size}>
                                <option>S</option>
                                <option>M</option>
                                <option>L</option>
                                <option>XL</option>
                                <option>XXL</option>
                            </select>
                        </div>
                        <p className="product-name">Rs. {element.price}</p>
                        <i className="fa fa-close" id="close-icon"><button className="close-button" onClick={ () => this.props.removeItem(element.itemid)}><IoIosClose /></button></i>
                    </div>
                );
                totalAmt = totalAmt + (element.price * element.quantity);
            });
            console.log(totalAmt);
        }


        if (this.props.items.hasOwnProperty("cartItems")) {
            if(this.props.items.cartItems.length > 0){
                return (
                    <div>
                        <div className="items-caption"><h4>Your cart</h4></div>
                        <div>
                            {products}
                        </div>
    
                        <div className="total-flex">
                            <div></div>
                            <div className="right">
                                <div className="total-amount">
                                    <div><p>Total:</p></div>
                                    <div><p>Rs. {totalAmt}</p></div>
                                </div>
                                <div className="checkout-button">
                                    <a href="">CHECK OUT</a>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
            else {
                return (
                    <div className="items-caption"><h4>No items</h4></div>
                );
            }            
        }else {
            return (
                <div className="items-caption"><h4>No items</h4></div>
            );
        } 
    }
}


function mapStateToProps(state, props) {
    { console.log("in maptostate " + state.products) }
    return {
        items: state.cartItems
    };
}
const mapDispatchToProps = (dispatch)=>({
    removeItem: (itemNumber)=>dispatch(removeItem(itemNumber))
});
export default connect(mapStateToProps, mapDispatchToProps)(CartPage);
