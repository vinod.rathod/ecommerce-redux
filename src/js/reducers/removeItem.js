import { REMOVE_ITEM } from '../constants/allConstants';

export default function removeItem(state = {},action){
    
    switch (action.type) {
        case REMOVE_ITEM:
            return {state, cartItems:action.payload};
            // break;
           
        default:
            return state;
    }

}