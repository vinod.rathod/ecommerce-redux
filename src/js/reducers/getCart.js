import { GET_MY_CART, REMOVE_ITEM } from '../constants/allConstants';

export default function getCart(state = {},action){
    
    switch (action.type) {
        case GET_MY_CART:
            return {state, cartItems:action.payload};
            break;
           
        case REMOVE_ITEM:
            return {state, cartItems:action.payload};
            break;

        default:
            return state;
    }

}