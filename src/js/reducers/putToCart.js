import { SEND_CART_ITEMS } from '../constants/allConstants';

export default function putToCart(state = {},action){
    
    switch (action.type) {
        case SEND_CART_ITEMS:
            return {state, putCartItemsStatus:action.payload};
            break;
           
        default:
            return state;
    }

}