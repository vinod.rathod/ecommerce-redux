import { combineReducers } from 'redux';
import allProducts from './getProducts';
import getCart from './getCart';
import putToCart from './putToCart';
import reoveItem from './removeItem';

const allReducers = combineReducers({
    products : allProducts,
    cartItems: getCart,
    sendCartItemsStatus: putToCart
});

export default allReducers;