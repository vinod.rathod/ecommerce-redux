import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import './App.css';

import MainMenu from './js/components/MainMenu';
import HomePage from './js/components/HomePage';
import GridComponent from './js/components/GridComponent';
import PaymentsPage from './js/components/PaymentsPage';
import CartPage  from './js/components/CartPage';
import Details from './js/components/Details';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <div className="Appp">
        <MainMenu></MainMenu>        
          <div>
            <div id="display-container">
              <Route exact path="/" component={HomePage} />
              <Route path="/products/:category" component={GridComponent} />
              <Route path="/payments" component={PaymentsPage} />
              <Route path="/cart" component ={CartPage} />
              <Route path="/details/:id" component = {Details} />
            </div>
          </div>        
      </div>
      </BrowserRouter>
    );
  }
}

export default App;
