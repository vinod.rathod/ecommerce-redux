var mysql = require('mysql');
var express = require('express');
var app = express();
var bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// app.use(express.bodyParser());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});

var con = mysql.createConnection({
    host: "localhost",
    user: "vinod",
    password: "password",
    database: "ecommerce",
});

app.get('/products', function (req, res) {
    var sql = "select id,name,url,price from products";
    con.query(sql, function (err, result, fields) {
        if (err) throw err;
        res.header('Content-type', 'application/json');
        res.json(result);
    });

});

app.get('/products/:category', function (req, res) {
    // console.log(req.params.category);
    var sql = "select id,name,url,price,description from products where category = ?";
    con.query(sql, [req.params.category], function (err, result, fields) {
        if (err) throw err;
        res.header('Content-type', 'application/json');
        res.json(result);
    })
});

app.post('/postitems', function (req, res) {
    console.log(req.body);
    if (!req.body.productId || !req.body.size || !req.body.quantity) {
        res.header('Content-type', 'application/json');
        res.json({ "message": "missing details" });
    } else {
        var item = req.body.productId;
        var quantity = req.body.quantity;
        console.log(req.body);
        var obj = {};
        var object = {};
        var sql = "select cart_id from customers_cart where user_id=1 and cart_status='pending'";
        con.query(sql, function (err, result, fields) {
            if (err) throw err;
            console.log(result.length);
            if (result.length > 0) {
                var pendingItemsQuery = `select item_id from cart_items  where cart_id = ? and item_status = 'not-incart'`;
                con.query(pendingItemsQuery, [result[0].cart_id], function (err, result1, fields) {
                    if (err) throw err;
                    if (result1.length > 0) {
                        var insertRemovedItemQuery = `update cart_items set size=?, quantity=?, item_status=?  where item_id=?`;
                        con.query(insertRemovedItemQuery, [req.body.size, req.body.quantity, 'incart', result1[0].item_id], function (err, result2, fields) {
                            if (err) throw err;
                            res.status(201);
                            res.header('Content-type', 'application/json');
                            res.json({ "message": "success" });
                        });
                    }
                    else {
                        var checkExistingQuery = `select item_id from cart_items  where cart_id = ? and product_id =? and item_status = 'incart'`;
                        con.query(checkExistingQuery, [result[0].cart_id, req.body.productId], function (err, results, fields) {
                            if (err) throw err;
                            if (results.length > 0) {
                                var updateQuery = `update cart_items set size = ?, quantity = ? where item_id = ?`;
                                con.query(updateQuery, [req.body.size, req.body.quantity, results[0].item_id], function (err, rs, fields) {
                                    if (err) throw err;
                                    res.status(201);
                                    res.header('Content-type', 'application/json');
                                    res.send({ "message": "success" });
                                });
                            } else {
                                var itemsInsertQuery = `insert into cart_items(cart_id,product_id,size,quantity,item_status) values(?,?,?,?,?)`;
                                con.query(itemsInsertQuery, [result[0].cart_id, req.body.productId, req.body.size, req.body.quantity, 'incart'], function (err, result3, fields) {
                                    if (err) throw err;
                                    res.status(201);
                                    res.header('Content-type', 'application/json');
                                    res.send({ "message": "success" });
                                });
                            }
                        });

                    }
                });
            } else {
                var sqlQuery = `insert into customers_cart(user_id, cart_status) values(1,'pending')`;
                con.query(sqlQuery, function (err, result1, fields) {
                    console.log(result[0])
                    if (err) throw err;
                    var cartIdQuery = `select cart_id from customers_cart where user_id = 1 and cart_status = 'pending'`;
                    con.query(cartIdQuery, function (err, result2, fields) {
                        if (err) throw err;
                        var itemsInsertQuery = `insert into cart_items(cart_id,product_id,size,quantity,item_status) values(?,?,?,?,?)`;
                        con.query(itemsInsertQuery, [result2[0].cart_id, req.body.productId, req.body.size, req.body.quantity, 'incart'], function (err, result3, fields) {
                            if (err) throw err;
                            res.status(201);
                            res.header('Content-type', 'application/json');
                            res.send({ "message": "success" });
                        });
                    });
                });
            }
        });

    }

});

app.get('/mycart/:userid', function (req, res) {
    console.log(req.params.userid);
    var object = {};
    var sql = `select p.id as productid,p.name,p.price,p.url,i.item_id as itemid,i.quantity,i.size from products as p, customers_cart as c,cart_items as i 
               where c.user_id = ? and c.cart_status = 'pending' and c.cart_id = i.cart_id and i.item_status = 'incart' and i.product_id = p.id`;
    // var sql = "select items from carts where cart_id = 21";
    con.query(sql, [req.params.userid], function (err, result, fields) {
        if (err) throw err;
        console.log(result.length);
        res.header('Content-type', 'application/json');
        res.send(result);
    })
});

app.post('/removeitems', function(req, res){
    if(!req.body.itemid){
        res.header('Content-type', 'application/json');
        res.send({"message" : "missing details"});
    }
    else{
        console.log("item id :"+req.body.itemid)
        var removeQuery = `update cart_items set item_status ='not-incart' where item_id = ?`;
        con.query(removeQuery, [req.body.itemid], function(err, result, fields){
            if(err) throw err;
            var sql = `select p.id as productid,p.name,p.price,p.url,i.item_id as itemid,i.quantity,i.size from products as p, customers_cart as c,cart_items as i 
               where c.user_id = 1 and c.cart_status = 'pending' and c.cart_id = i.cart_id and i.item_status = 'incart' and i.product_id = p.id`;
                // var sql = "select items from carts where cart_id = 21";
                con.query(sql, function (err, result1, fields) {
                    if (err) throw err;
                    // console.log(result.length);
                    res.header('Content-type', 'application/json');
                    res.send(result1);
                })
            // res.header('Content-type', 'application/json');
            // res.send({"message" : "success"});
        });
    }
});

app.listen(8000, function () {
    console.log("server started on port 8000");
})
